#include "mesh.h"

/**
 * @brief Construct a cone, used to represent the light source.
 * @return A cone without a base.
 */
Mesh init_light_base();

/**
 * @brief Construct the base of the cone returned by init_light_base.
 * @return A simple disk.
 */
Mesh init_light_emitter();

/**
 * @brief Construct a sphere.
 */
Mesh init_sphere();
