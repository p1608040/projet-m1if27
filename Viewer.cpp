#include "draw.h"
#include "texture.h"
#include "uniforms.h"
#include "wavefront.h"
#include "window.h"
#include "meshes.h"
#include "Viewer.h"
#include <cmath>
#include <experimental/random>

// Make a window of size width * height
Viewer::Viewer(const int width, const int height) : App(width, height) {}

// Init OpenGL state and variables for rendering with shadow map.
void Viewer::init_shadowMapping() {
    glEnable(GL_CULL_FACE); // Enable culling. Used for Peter Panning

    // Configure shadow texture
    this->depthMapWidth = window_width() * 2;
    this->depthMapHeight = window_height() * 2;

  

    // Generate and configure framebuffer and shadow map texture
    glGenFramebuffers(1, &this->framebuffer);
    glGenTextures(1, &this->depthMap);
    glBindTexture(GL_TEXTURE_2D, this->depthMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, this->depthMapWidth,
        this->depthMapHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Attach depth map to frame buffer and disable colors
    glBindFramebuffer(GL_FRAMEBUFFER, this->framebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
        this->depthMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Solve over sampling (i.e. far regions considered as in shadow)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    const float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    // Init shaders
    this->program_shadow_map = read_program("projet-m1if27/shaders/shadow_map.glsl");
    program_print_errors(this->program_shadow_map);
    this->program_render_map = read_program("projet-m1if27/shaders/render_map.glsl");
    program_print_errors(this->program_render_map);
}

// Creation of OpenGL objects and OpenGL early configurations
int Viewer::init() {
    // Basic configuration of OpenGL
    glClearColor(0.2f, 0.2f, 0.2f, 1.f); // Default window color
    glClearDepth(1.f); // Default depth
    glDepthFunc(GL_LESS); // Ztest : Keep the closest intersection to camera
    glEnable(GL_DEPTH_TEST); // Enable ztest

    widgets = create_widgets();
    click = 0;
    click2 = 0;
    click3 = 0;
    click4 = 0;
    click5 = 0;
    click6 = 0;
    click7 = 0;
    click8 = 0;
    click9 = 0;

    this->bigguy = read_mesh("data/bigguy.obj");
    this->sphere = init_sphere();
    this->cube = read_mesh("data/cube.obj");
    this->palm = read_mesh("projet-m1if27/data/palm/Palm_01.obj");
    this->bat = read_mesh("projet-m1if27/data/bat/15250_Key_Ring_Wall_Mount-Bat_NEW.obj");
    //this->chair = read_mesh("projet-m1if27/data/chair/Chair.obj");
    this->hand1 = read_mesh("projet-m1if27/data/hand1/16834_hand_v1_NEW.obj");
    this->hand2 = read_mesh("projet-m1if27/data/hand2/15800_Mummy_Hand_v1_NEW.obj");
    this->hand3 = read_mesh("projet-m1if27/data/hand3/11537_Hand_v3.obj");


    // Place camera
    this->camera.lookat(Point(0, 0, 0), 100);

    // Place light
    this->lightCamera.lookat(Point(0, 0, 0), 50);

    // Init mesh
  
    m_sceneObjects = {
        {
            this->bigguy ,
            Translation(1, -5, -12) * Scale(0.5, 0.5, 0.5)* RotationY(180)
        },
        {
            this->cube,
            Scale(100, 100, 0.5)
        },
        {
            this->cube,
            Translation(0, -10, -10) * Scale(20, 0.5, 20)
        },
        {
            init_light_base(),
            Identity()
        }
    };

    this->lightTransform = &this->m_sceneObjects.at(3).m_transform;
    updateLightTransform();

    std::cout << "ScenObejcts : " << m_sceneObjects.size() << std::endl;

    this->lightEmitter = init_light_emitter();

    // Init other shaders
    this->program_light_emitter = read_program("projet-m1if27/shaders/light_emitter.glsl");
    program_print_errors(this->program_light_emitter);

    // Initialize shadow mapping and rendering shaders.
    this->init_shadowMapping();

    return 0;
}

// Destruction of OpenGL objects
int Viewer::quit() {

    // Release meshes
    m_sceneObjects.clear();
    m_artObjects.clear();
    this->lightEmitter.release();

    // Release buffers
    glDeleteFramebuffers(1, &this->framebuffer);
    glDeleteTextures(1, &this->depthMap);

    // Release shaders
    release_program(this->program_shadow_map);
    release_program(this->program_render_map);
    release_program(this->program_light_emitter);

    release_widgets(widgets);

    return 0;
}

//Create art construction blocks
void Viewer::addArtElement(const Mesh & blockMesh, const Transform & blockTransform) {
    auto obj =std::unique_ptr<Object>(new Object(blockMesh, blockTransform));
    m_artObjects.emplace_back(std::move(obj));
}

// Rendering step 1 : render depth map
void Viewer::drawMeshStep1() {
    for ( auto & object : m_sceneObjects)
        drawMeshStep1(object);
    
    for ( auto & object : m_artObjects)
        drawMeshStep1(*(object.get()));
}

void Viewer::drawMeshStep1( Object & object) {
    drawMeshStep1(object.m_mesh,object.m_transform);
}

void Viewer::drawMeshStep1(Mesh& mesh, const Transform& model) {
    // Declare shader to use
    glUseProgram(this->program_shadow_map);

    // Get uniforms
    const Transform view = this->lightCamera.view();
    const Transform projection = this->lightCamera.projection(
        this->depthMapWidth, this->depthMapHeight, 90);
    const Transform mvp = projection * view * model;

    // Send uniforms
    program_uniform(this->program_shadow_map, "mvpMatrix", mvp);

    // Draw !
    mesh.draw(this->program_shadow_map, true, true, true, false);
}

// Rendering step 2 : render window
void Viewer::drawMeshStep2() {
    for ( auto & object : m_sceneObjects)
        drawMeshStep2(object);

    for ( auto & object : m_artObjects)
        drawMeshStep2(*(object.get()));
}

void Viewer::drawMeshStep2( Object & object){
    drawMeshStep2(object.m_mesh,object.m_transform);
}

void Viewer::drawMeshStep2(Mesh& mesh, const Transform& model) {
    // Declare shader to use
    glUseProgram(this->program_render_map);

    // Get uniforms
    const Transform view = this->camera.view();
    const Transform projection
        = this->camera.projection(window_width(), window_height(), 45);
    const Transform lightView = this->lightCamera.view();
    const Transform lightSpace = this->lightCamera.projection(
        this->depthMapWidth, this->depthMapHeight, 90) * lightView;

    // Send uniforms for vertex shader
    program_uniform(this->program_render_map, "model", model);
    program_uniform(this->program_render_map, "view", view);
    program_uniform(this->program_render_map, "projection", projection);
    program_uniform(this->program_render_map, "lightSpaceMatrix", lightSpace);

    // Send uniforms for fragment shader
    program_use_texture(this->program_render_map, "shadowMap", 0, this->depthMap, 0);
    program_uniform(this->program_render_map, "lightPosition", this->lightCamera.position());
    program_uniform(this->program_render_map, "viewPosition", this->camera.position());

    // Draw !
    mesh.draw(this->program_render_map, true, true, true, false);
}

// Zoom and move camera with mouse.
void Viewer::controlCamera() {
    // Move
    int mx, my;
    unsigned int mb = SDL_GetRelativeMouseState(&mx, &my);
    if (mb & (SDL_BUTTON(1) | SDL_BUTTON(3))) // Mouse left and right buttons
        this->camera.rotation(mx, my);
    else if (mb & SDL_BUTTON(2)) // Mouse middle button
        this->camera.translation(
            (float) mx / (float) window_width(), 
            (float) my / (float) window_height()
        );

    // Zoom
    SDL_MouseWheelEvent wheel = wheel_event();
    if (wheel.y != 0) {
        clear_wheel_event();
        this->camera.move(8.f * wheel.y);
    }
}

//Select an object
/*
void Viewer::SelectObject() {

}
*/

//void Viewer::MoveObject()

void Viewer::controlLastObject() {
    if (!this->m_artObjects.empty()) {
        const float factor = 0.25;
        if (key_state(SDLK_LSHIFT)) {
            if (key_state(SDLK_UP)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Translation(0, factor, 0);
            }
            if (key_state(SDLK_DOWN)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Translation(0, -factor, 0);
            }
            if (key_state(SDLK_LEFT)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Translation(factor, 0, 0);
            }
            if (key_state(SDLK_RIGHT)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Translation(-factor, 0, 0);
            }
            if (key_state(SDLK_p)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Translation(0, 0, factor);
            }
            if (key_state(SDLK_m)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Translation(0, 0, -factor);
            }
        }
    }
}

void Viewer::rotateLastObject() {
    if (!this->m_artObjects.empty()) {
        const float factor = 2;
        if (key_state(SDLK_LCTRL)) {
            if (key_state(SDLK_x)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * RotationX(factor);
            }
            if (key_state(SDLK_y)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * RotationY(factor);
            }
            if (key_state(SDLK_z)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * RotationZ(factor);
            }
        }
    }
}

void Viewer::resizeLastObject() {
    if (!this->m_artObjects.empty()) {
        const float factor = 1.25;
        const float factor2 = 0.85;
        if (key_state(SDLK_LALT)) {
            if (key_state(SDLK_UP)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(1, factor, 1);
            }
            if (key_state(SDLK_DOWN)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(1, factor2, 1);
            }
            if (key_state(SDLK_LEFT)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(factor, 1, 1);
            }
            if (key_state(SDLK_RIGHT)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(factor2, 1, 1);
            }
            if (key_state(SDLK_p)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(1, 1, factor);
            }
            if (key_state(SDLK_m)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(1, 1, factor2);
            }
            if (key_state(SDLK_m)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(1, 1, factor2);
            }
                if (key_state(SDLK_s)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(factor2, factor2, factor2);
            }
            if (key_state(SDLK_b)) {
                Object& obj = *this->m_artObjects.back();
                obj.m_transform = obj.m_transform * Scale(factor, factor, factor);
            }

        }
    }
}

// Move light with keyboard.
void Viewer::controlLight() {
    const float factor = 0.01;
    if (!key_state(SDLK_LCTRL) && !key_state(SDLK_LALT) && !key_state(SDLK_LSHIFT)) {
        if (key_state(SDLK_LEFT)) {
            this->lightCamera.translation(factor, 0);
            updateLightTransform();
        }
        if (key_state(SDLK_RIGHT)) {
            this->lightCamera.translation(-factor, 0);
            updateLightTransform();
        }
        if (key_state(SDLK_UP)) {
            this->lightCamera.translation(0, factor);
            updateLightTransform();
        }
        if (key_state(SDLK_DOWN)) {
            this->lightCamera.translation(0, -factor);
            updateLightTransform();
        }
        if (key_state(SDLK_p)) {
            this->lightCamera.move(50 * factor);
            updateLightTransform();
        }
        if (key_state(SDLK_m)) {
            this->lightCamera.move(-50 * factor);
            updateLightTransform();
        }
    }
}

// To call when light is moved.
void Viewer::updateLightTransform() {
    *this->lightTransform = Translation(Vector(this->lightCamera.position()))
        * RotationX(-90);
}

// Draw emitter part of light
void Viewer::drawLightEmitter(const Transform& model) {
    glUseProgram(this->program_light_emitter);

    // Get uniforms
    const Transform view = this->camera.view();
    const Transform projection
        = this->camera.projection(window_width(), window_height(), 45);
    const Transform mvp = projection * view * model;

    // Send uniforms
    program_uniform(this->program_light_emitter, "mvpMatrix", mvp);

    // Draw !
    this->lightEmitter.draw(this->program_light_emitter, true, false, false, false);
}

// Draw new frame
int Viewer::render() {
    // *** Handle events *** //

    // Camera control
    this->controlCamera();

    // Light control
    this->controlLight();

    this->controlLastObject();
    this->resizeLastObject();
    this->rotateLastObject();

    // *** Render objects *** //

    // Step 1 : render to depth map //
    glViewport(0, 0, this->depthMapWidth, this->depthMapHeight);
    glBindFramebuffer(GL_FRAMEBUFFER, this->framebuffer);
    glClear(GL_DEPTH_BUFFER_BIT);
    glCullFace(GL_FRONT); // Help against Peter panning : change culling

    // Draw meshes
    this->drawMeshStep1();

    glCullFace(GL_BACK); // Reset culling
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Step 2 : render scene using depth map //
    glViewport(0, 0, window_width(), window_height());
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, this->depthMap);

    // Draw meshes
    this->drawMeshStep2();
    
    // Draw light emitter

    this->drawLightEmitter(*this->lightTransform);


    begin(widgets);
    begin_line(widgets);
    label(widgets, "cube");
    button(widgets, "create cube", click);
    if(click) {        
        // addArtElement(this->cube, Translation(10+std::rand()*1.0/RAND_MAX*10, -5 +std::rand()*1.0/RAND_MAX*10, -12) );
        addArtElement(this->cube, Translation(10, -5, -12) );
        begin_line(widgets);
        label(widgets, "created a cube");
        click=0;
    }
 
    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "sphere");
    button(widgets, "create sphere", click2);
    if(click2) {
        std::cout << "inside btn 2 handler" << std::endl;
        addArtElement(this->sphere, Translation(15+std::rand()*1.0/RAND_MAX*10, -5+std::rand()*1.0/RAND_MAX*10, -12) * Scale(2, 2, 2) );
        //addArtElement(this->sphere, Translation(15, -5, -12) * Scale(2, 2, 2) );
        begin_line(widgets);
        label(widgets, "created a sphere");
        click2=0;

    }

    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "palm");
    button(widgets, "create palm tree", click3);
    if(click3) {
        addArtElement(this->palm, Translation(20, -5, -12));
        begin_line(widgets);
        label(widgets, "created a palm tree");
        click3=0;
    }

    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "bat");
    button(widgets, "create bat", click4);
    if(click4) {
        addArtElement(this->bat, Translation(20, -5, -12));
        begin_line(widgets);
        label(widgets, "created a bat");
        click4=0;
    }

    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "hand1");
    button(widgets, "create hand1", click5);
    if(click5) {
        addArtElement(this->hand1, Translation(20, -5, -12));
        begin_line(widgets);
        label(widgets, "created a hand1");
        click5=0;
    }

    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "hand2");
    button(widgets, "create hand2", click6);
    if(click6) {
        addArtElement(this->hand2, Translation(20, -5, -12));
        begin_line(widgets);
        label(widgets, "created a hand2");
        click6=0;
    }

    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "hand3");
    button(widgets, "create hand3", click7);
    if(click7) {
        addArtElement(this->hand3, Translation(20, -5, -12));
        begin_line(widgets);
        label(widgets, "created a hand3");
        click7=0;
    }

    begin_line(widgets);
    begin_line(widgets);
    begin_line(widgets);
    begin_line(widgets);
    label(widgets, "delete");
    
    button(widgets, "delete the last object", click8);
    if(click8) {
        if (!this->m_artObjects.empty()){
            this->m_artObjects.pop_back();
            begin_line(widgets);
            label(widgets, "deleted the last object");
            click8=0;
        }
    }

    button(widgets, "delete all objects", click9);
    if(click9) {
        if (!this->m_artObjects.empty()){
            this->m_artObjects.clear();
            begin_line(widgets);
            label(widgets, "deleted all objects");
            click9=0;
        }
    }

    end(widgets);
    
    // afficher l'interface
    draw(widgets, window_width(), window_height());
    this->drawLightEmitter(*this->lightTransform);
 
    return 1; // Error = 0
}
