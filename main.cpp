#include "Viewer.h"

/**
 * @brief Entry point of the program.
 * @return 0 if everything is OK 
 */
int main() {
    Viewer v;
    v.run();

    return 0;
}
