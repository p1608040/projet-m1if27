#version 330

// *** VERTEX_SHADER *** //

#ifdef VERTEX_SHADER
layout(location = 0) in vec3 position;

uniform mat4 mvpMatrix;

void main() {
    gl_Position = mvpMatrix * vec4(position, 1);
}
#endif


// *** FRAGMENT_SHADER *** //

#ifdef FRAGMENT_SHADER
// We only want depth, so we need to perform `gl_FragDepth = gl_FragCoord.z;`
// but it is already automatically done.
void main() {}
#endif
