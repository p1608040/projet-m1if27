#version 330

/* *** VERTEX SHADER *** */
#ifdef VERTEX_SHADER
// Basic position calculation with MVP

layout(location = 0) in vec3 position;

uniform mat4 mvpMatrix;

void main() {
    gl_Position = mvpMatrix * vec4(position, 1);
}
#endif

/* *** FRAGMENT_SHADER *** */
#ifdef FRAGMENT_SHADER
// Make all pixels white

out vec4 fragmentColor;

void main() {
    fragmentColor = vec4(1, 1, 1, 1);
}
#endif
