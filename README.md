# Projet M1if27

Le repository ne contient que le code nécessaire au projet. Pour pouvoir le compiler avec gkit, il faut suivre quelques étapes.

## Rapport

Un rapport est disponible à l'adresse **rapport/rapport_Gabriella_PETHO_Thomas_PEYROT.pdf**.

## Installation

### Cloner et obtenir les fichiers

Cloner le repository dans celui de gkit2light.
`cd path/to/gkit2light/` puis `git clone git@forge.univ-lyon1.fr:p1608040/projet-m1if27.git` ou `git clone https://forge.univ-lyon1.fr/p1608040/projet-m1if27.git`.

Après ces étapes, on obtient un dossier nommé `projet-m1if27` à la racine du dépôt gkit2light.

### Séparer les deux repositories

Il faut "séparer" les deux dépôts afin de ne commit que ce qu'il faut.
Pour cela ajouter au fichier `.gitignore` de gkit2light la ligne `projet-m1if27/`.

## Compiler

Ajouter à la fin du fichier `premake4.lua`.

```lua
-- projet de l'UE Synthèse d'Images
project("shadow_art")
    language "C++"
    kind "ConsoleApp"
    targetdir "bin"
    files ( gkit_files )
    files { gkit_dir .. "/projet-m1if27/*.cpp" }
```

Ensuite, à la racine de gkit2light, faire `premake4 gmake`.

À partir de maintenant, il sera possible de compiler le projet, par la commandes `make shadow_art` (ou bien la commande `make` qui compile tous les fichiers). L'exécutable sera disponible dans le dossier `bin/`.

## Exécuter

Après compilation, depuis la racine de gkit2light, `bin/shadow_art`.

## Contrôles

**Déplacez la caméra avec la souris.**<br />
Changez l'orientation par rapport au centre de l'image avec le bouton gauche ou droit.<br />
Faites un déplacement panoramique en cliquant sur le bouton molette tout en déplaçant la souris.<br />
Zommez/Dézoomez avec la molette.

**Déplacez la lumière au clavier.**<br />
Vous pouvez déplacer la lumière dans les deux directions parallèles au mur grâce aux touches *flèches* de votre clavier, et également perpendiculairement grâce aux touches *P* et *M*.

**Créez des objets à la souris**<br/>
Vous pouvez cliquer sur un des boutons en haut à gauche pour créer un objet.

**Déplacez le dernier objet créé au clavier**<br/>
Vous pouvez utiliser les flèches de votre clavier et les touches *P* et *M* en combinaison avec la touche *Majuscule gauche* pour déplacer le dernier objet créé, semblablement au déplacement de la lumière.

**Changez la taille du dernier objet créé au clavier**<br/>
Vous pouvez utiliser les flèches de votre clavier et les touches *P* et *M* en combinaison avec la touche *Alt gauche* pour changer la taille du dernier objet créé.

**Faites tourner le dernier objet créé au clavier**<br/>
En appuyant sur la touche *Ctrl gauche* et une autre parmie *x*, *y* et *z*, vous pouvez faire tourner l'objet dans un axe.
