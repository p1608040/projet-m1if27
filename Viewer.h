#ifndef __PROJET_MIF27_VIEWER_84139434867616_
#define __PROJET_MIF27_VIEWER_84139434867616_

#include "app.h"
#include "orbiter.h"
#include "mesh.h"
#include <vector>
#include <memory>
#include "widgets.h"

struct Object {
public:
    Object(const Mesh & mesh, const Transform & transform)
    : m_mesh(mesh)
    , m_transform(transform) {
        std::cout << "Object sizeof_mesh : " << sizeof(mesh) << std::endl;
    }

    ~Object(){
        std::cout << "~Object" << std::endl; 
        m_mesh.release();
    }
//private:
    Mesh m_mesh;
    Transform m_transform;
};

/**
 * @brief All OpenGL processes are managed from this class.
 */
class Viewer : public App {
public:
    /**
     * @brief Construct a Window of size width * height.
     * @param [in] width Width of the window.
     * @param [in] height Height of the window.
     */
    Viewer(const int width = 1024, const int height = 768);

private:

    /**
     * @brief UI widget variables
     */

    Widgets widgets;
    int click;
    int click2;
    int click3;
    int click4;
    int click5;
    int click6;
    int click7;
    int click8;
    int click9;

    /**
     * @brief The camera showing the scene.
     */
    Orbiter camera;

    /**
     * @brief Mesh objects of the scene/art building blocks and their transformations;
     */
    Mesh bigguy;
    Mesh sphere;
    Mesh cube;
    Mesh palm;
    Mesh bat;
    //Mesh chair;
    Mesh hand1;
    Mesh hand2;
    Mesh hand3;


    Mesh lightBase;
    Mesh lightEmitter;
    Transform* lightTransform;

    std::vector<Object> m_sceneObjects;
    std::vector<std::unique_ptr<Object>> m_artObjects;
    

    /**
     * @brief Variables used to render meshes with shadow mapping.
     */
    int depthMapWidth; // Width of depth map.
    int depthMapHeight; // Height of depth map.
    GLuint framebuffer; // Framebuffer required for depthMap.
    GLuint depthMap; // A texture storing the shadows.
    Orbiter lightCamera; // A light acting as a camera because of shadow mapping.
    GLuint program_shadow_map; // Shader for the first step of the multi-pass rendering.
    GLuint program_render_map; // Shader for the second step of the multi-pass rendering.

    /**
     * @brief A shader used to display the light itself.
     */
    GLuint program_light_emitter;

    /**
     * @brief List of building blocks
     */
    //List<buildingBlock> buildingBlocks;


    /**
     * @brief All initializations are made in Viewer::init().
     * @return 0 if everything is OK.
     */
    int init();
    /**
     * @brief Release what should be released before quitting the program.
     * @return 0 if everything is OK.
     */
    int quit();
    /**
     * @brief Function called at each frame to render the new one.
     * @return 1 if everything is OK.
     * @return 0 if there is an error. It stops the program.
     */
    int render();

    /**
     * @brief Called at initialization. Configure shadow mapping and rendering shaders.
     */
    void init_shadowMapping();

    /**
     * @brief Function to create new objects to construct the art piece.
     */
    void addArtElement(const Mesh & blockMesh, const Transform & blockTransform);

    /**
     * @brief Render Mesh with desired transform in step 1 of rendering.
     * @param mesh A Mesh object you want to draw.
     * @param model A Transformation to apply to the mesh.
     */
    void drawMeshStep1( Mesh& mesh, const Transform& model);
    void drawMeshStep1( Object & object);
    void drawMeshStep1();
    /**
     * @brief Render Mesh with desired transform in step 2 of rendering.
     * @param mesh A Mesh object you want to draw.
     * @param model A Transformation to apply to the mesh.
     */
    void drawMeshStep2(Mesh& mesh, const Transform& model);
    void drawMeshStep2( Object & object);
    void drawMeshStep2();
    /**
     * @brief Use this->program_light_emmitter to render light emitter.
     * @param model A Transformation to apply to the emitter (used with MVP).
     */
    void drawLightEmitter(const Transform& model);

    /**
     * @brief Control camera thanks to mouse.
     */
    void controlCamera();

    /**
     * @brief Move light with keyboard.
     */
    void controlLight();

    /**
     * @brief Move the last building block added.
     */
    void controlLastObject();

    /**
     * @brief Resize the last building block added.
     */
    void resizeLastObject();

    /**
     * @brief Rotate the last building block added.
     */
    void rotateLastObject();

    /**
     * @brief To use when light is moved.
     */
    void updateLightTransform();
};

#endif
