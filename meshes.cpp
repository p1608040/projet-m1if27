#include <cmath>
#include "meshes.h"

// Construct mesh representing light source.
Mesh init_light_base() {
    Mesh cone = Mesh(GL_TRIANGLE_STRIP);

    const int div = 25;
    const float step = 2 * M_PI / div;
    for (int i = 0; i <= div; i++) {
        const float alpha = i * step;
        cone.normal(Vector(cos(alpha) / sqrt(2), 1 / sqrt(2), sin(alpha) / sqrt(2)));
        cone.texcoord(sin(alpha/4), 0);
        cone.vertex(Point(cos(alpha), 0, sin(alpha)));
        cone.texcoord(0.5, 1);
        cone.vertex(Point(0, 1, 0));
    }

    return cone;
}

// Construct a cone representing light emitter.
Mesh init_light_emitter() {
    Mesh disk = Mesh(GL_TRIANGLE_FAN);

    const int div = 25;
    const float step = 2 * M_PI / div;
    disk.normal(Vector(0, 1, 0));
    for (int i = 0; i <= div; i++) {
        const float alpha = i * step;
        disk.vertex(Point(cos(alpha), 0, sin(alpha)));
    }

    return disk;
}

Mesh init_sphere() {
    Mesh sphere = Mesh(GL_TRIANGLE_STRIP);

    int divBeta = 26;
    int divAlpha = divBeta / 2;
    float alpha, alpha2, beta;

    for (int i = 0; i < divAlpha; i++) {
        alpha = -0.5 * M_PI + (float(i) * M_PI) / divAlpha;
        alpha2 = -0.5 * M_PI + (float(i+1) * M_PI) / divAlpha;
        for (int j = 0; j <= divBeta; j++) {
            beta = float(j) * 2.f * M_PI/ (divBeta-1);
            sphere.normal(Vector(cos(alpha) * cos(beta), sin(alpha), cos(alpha) * sin(beta)));
            sphere.texcoord(beta / (2 * M_PI), 0.5 + alpha / M_PI);
            sphere.vertex(Point(cos(alpha)*cos(beta), sin(alpha), cos(alpha)*sin(beta)));
            sphere.normal(Vector(cos(alpha2) * cos(beta), sin(alpha2), cos(alpha2) * sin(beta)));
            sphere.texcoord(beta / (2 * M_PI), 0.5 + alpha2 / M_PI);
            sphere.vertex(Point(cos(alpha2) * cos(beta), sin(alpha2), cos(alpha2) * sin(beta)));
        }
    }

    return sphere;
}
