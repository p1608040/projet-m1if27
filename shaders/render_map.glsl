#version 330

// *** VERTEX_SHADER *** //

#ifdef VERTEX_SHADER
// Inputs
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texcoords;
layout(location = 2) in vec3 normal;

// Uniforms
uniform mat4 model; // MVP matrix : M
uniform mat4 view; // MVP matrix : V
uniform mat4 projection; // MVP matrix : P
uniform mat4 lightSpaceMatrix; // VP (as in MVP) matrix from light's space

// Outputs
out VS_TO_FS {
    vec3 fragmentPosition;
    vec3 normal;
    vec2 texcoords;
    vec4 fragmentPositionLightSpace;
} vs_out;

void main() {
    vs_out.fragmentPosition = vec3(model * vec4(position, 1.0));
    vs_out.normal = normalize(transpose(inverse(mat3(model))) * normal);
    vs_out.texcoords = texcoords;
    vs_out.fragmentPositionLightSpace = lightSpaceMatrix * vec4(vs_out.fragmentPosition, 1.0);
    gl_Position = projection * view * vec4(vs_out.fragmentPosition, 1.0);
}
#endif


// *** FRAGMENT_SHADER *** //

#ifdef FRAGMENT_SHADER
// Inputs
in VS_TO_FS {
    vec3 fragmentPosition;
    vec3 normal;
    vec2 texcoords;
    vec4 fragmentPositionLightSpace;
} fs_in;

// Uniforms
// Next line will be used when the shader will support textures
//uniform sampler2D diffuseTexture;
uniform sampler2D shadowMap; // Shadow map containing depths of fragments
uniform vec3 lightPosition; // Position of the light
uniform vec3 viewPosition; // Position of the camera

// Outputs
out vec4 fragColor;

// Return a ratio from dot product used to make a color gradient
// Ratio consider a gradient from 0° to 45°
float getRatioDotProduct(const float dot_product) {
    const float kst = 2.0 / (sqrt(2.0) - 2.0);
    return kst * dot_product - kst;
}

// If there is no shadow, returns 0
// If there is a shadow, returns a number between 0 and 1 to match a color gradient of 45°
float calculateShadow(const vec4 fragmentPositionLightSpace, const vec3 fragmentNormal,
    const vec3 lightDirection, const float lightDotProduct) {
    // Perspective division
    vec3 projectionCoordinates
        = fragmentPositionLightSpace.xyz / fragmentPositionLightSpace.w;
    // Bind values to [0, 1]
    projectionCoordinates = projectionCoordinates * 0.5 + 0.5;
    // Get closest depth value from shadow map
    float closestDepth = texture(shadowMap, projectionCoordinates.xy).r;
    // Get current depth value from shadow map
    float currentDepth = projectionCoordinates.z;

    // Compare thoses depths to determine if fragment is in shadow
    float shadow = 0.0;
    if (projectionCoordinates.z <= 1.0) { // If not, then it is not in shadow
        float bias = 0; // Bias is used to avoid shadow acne
        // Bias is determined based on directions to avoid Peter Panning
        float dotProductFragment = dot(fragmentNormal, lightDirection);
        if (dotProductFragment < (sqrt(3) / 2)) { // Superior to 30°
            bias = 0.001;
        }
        if (currentDepth - bias > closestDepth) {
            // If fragment is NOT the closest point to light,
            // then it is in shadow
            shadow = 1.0;
        } else {
            // If we are NOT in shadow, we still draw as shadow
            // fragments at more than 45° away from light normal.
            // We use a ratio to make a gradient,
            // to smoothly go from full light to full shadow.
            float dotRatio = getRatioDotProduct(lightDotProduct);
            shadow = min(dotRatio, 1.0); // Shadow can't be superior to 1
        }
    }

    return shadow;
}

void main() {
    // Next line will be used when the shader will support textures
    //vec3 color = texture(diffuseTexture, fs_in.TexCoords).rgb;
    const vec3 meshColor = vec3(0.9); // Mesh color
    const vec3 lightColor = vec3(1.0); // Light color
    // Ambient
    vec3 ambient = 0.15 * meshColor;
    // Diffuse
    vec3 lightDirection = normalize(lightPosition - fs_in.fragmentPosition);
    float diff = max(dot(lightDirection, fs_in.normal), 0.0);
    vec3 diffuse = diff * lightColor;
    // Specular
    vec3 viewDirection = normalize(viewPosition - fs_in.fragmentPosition);
    vec3 halfwayDirection = normalize(lightDirection + viewDirection);
    float spec = pow(max(dot(fs_in.normal, halfwayDirection), 0.0), 64.0);
    vec3 specular = 0.1 * spec * lightColor;
    // Calculate shadow
    const vec3 lightNormal = vec3(0, 0, -1);
    float shadow = calculateShadow(fs_in.fragmentPositionLightSpace,
        fs_in.normal, lightDirection, dot(lightNormal, lightDirection));
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * meshColor;

    fragColor = vec4(lighting, 1.0);
}
#endif
